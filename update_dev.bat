set PYTHON_3_EXE=c:/Python35/python.exe
set JPEGTURBO_DIR=c:/libjpeg-turbo64

cd c://sa
git pull

set /p BRANCH="Enter branch you want to build: "

if not DEFINED BRANCH (
	set  BRANCH=2.x
)

git checkout "%BRANCH%"

start  "SA_WEB_DEV_BUILD" c:\sa\build.bat
